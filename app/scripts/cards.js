console.log('cargando Cards...');
const dataCards = [{
    "title": "Receta de Tres Leches",
    "url_image":"https://spooncr.com/Images/Products/7856_132641101546862926.jpg",
    "desc":"receta de postres",
    "cta":"Show More",
    "link":"https://www.recetasgratis.net/receta-de-torta-tres-leches-8910.html"
},
{
    "title": "Receta de Queques",
    "url_image":"https://i.pinimg.com/originals/a9/0b/f2/a90bf2d009044a940b1294651ffbfa58.jpg",
    "desc":"receta postres",
    "cta":"Show More",
    "link":"https://as.com/meristation/juegos/top/videojuegos_accion/"
},
{
    "title": "Mejores juegos para PS4 2021",
      "url_image":"https://i.ytimg.com/vi/pf2hRgGviR8/maxresdefault.jpg",
      "desc":"No deseches esa ps4 atrevete a mirar este top game de mejores videojuegos para la consola y aprovechala al maximo.",
      "cta":"Show More",
      "link":"https://www.hobbyconsolas.com/reportajes/mejores-juegos-ps4-121442"
},
{
      "title": "Juegos para  mobiles adroid ",
      "url_image":"https://www.proandroid.com/wp-content/uploads/2018/05/mejores-juegos-android-gratis-2018.jpg?mrf-size=m",
      "desc":"Te demostramor porque adroid tiene los mejores juegos mobiles",
      "cta":"Show More",
      "link":"https://www.xatakandroid.com/listas/mejores-juegos-para-android-2020"
},
{
      "title": "Recuerda los años de antiguos mejores juegos retro!!",
      "url_image":"https://www.headsem.com/wp-content/uploads/2017/12/ConsolasRetro.jpg",
      "desc":"No es necesario una ps5 para poder disfrutar de los mejores videojuegos que alguno de nosotros jugamos alguna vez.",
      "cta":"Show More",
      "link":"https://www.hobbyconsolas.com/videojuegos/top-juegos/retro"
},
{
      "title": "Mejores juegos para la Xbox 360",
      "url_image":"https://i.ytimg.com/vi/gFJVH19VAvc/hqdefault.jpg",
      "desc":"En el siguiente link podréis ver nuestro top 10 de los mejores juegos para Xbox 360. Una lista hecha con el corazón de dejando de lado lo preestablecido.",
      "cta":"Show More",
      "link":"https://as.com/meristation/plataformas/xbox_one/top/"

}]; 

(function (){
  let CARD = {
     init: function (){
    //console.log ('card module was loaded');
     let _self = this;
//llamamos las funciones
    this.inserData(_self);
//this.eventHandler(_self);
},

EventHandler: function (_self) {
let arrayRefs = document.querySelectorAll('.accordion-title');
for (let x = 0; x < arrayRefs.length; x++) {
    arrayRefs[x].addEventListener('click', function(event){
          console.log('event',event);
          _self.showTab(event.target);  
    });
}
},
inserData: function (_self) {
    dataCards.map(function (item, index){
    document.querySelector('.card-list').insertAdjacentHTML('beforeend', _self.tplCardItem(item, index));
    });

    },

tplCardItem: function (item, index) {
return(`<div class='card-item' id="card-number-${index}">
<img src="${item.url_image}"/>
<div class = "card-info">
<p class ='card-title'>${item.title}</p>
<p class ='card-desc'>${item.desc}</p>
<a class ='card-cta' target="blank" href="${item.link}">${item.cta}</a>
</div>
</div>`)},

}

CARD.init();
})();